/* IMPORTS DOS MÓDULOS USADOS NO PROGRAMA */
use std::{thread, time};
use rand::Rng;
extern crate term_size;

/* ESTRUTURA DE CÉLULA PARA AUTÔMATO */
#[derive(Copy, Clone)]
struct CellStruct {
    state: bool,            // 1 - Viva, 0 - Morta.
}

/* DEFINIÇÃO DOS MÉTODOS EXCLUSIVOS DE CADA CÉLULA, COM INICIALIZAÇÃO */
impl CellStruct {
    fn new () -> CellStruct {
        /*
         *          RESUMO:
         *
         *  Método construtor para a estrutura
         *  de célula usada no arranjo.
         *
         */ 

        CellStruct {
            state: true, 
        }
    }

    fn init_rand (x_dim: usize, y_dim: usize) -> Vec<Vec<CellStruct>> {
        /*
         *          RESUMO:
         *
         *  Função de inicialização aleatória
         *  para a grade de células inicial.
         *
         */ 

        let mut grid = vec![vec![CellStruct::new();  y_dim]; x_dim];
        let mut rng = rand::thread_rng();
        for row in grid.iter_mut() {
            for cell in row.iter_mut() {
                cell.state = rng.gen::<bool>();
            }
        }

        grid
    }
}

/* ESTRUTURA DE GRADE DE CÉLULAS */
#[derive(Clone)]
struct Grid {
    grid:       Vec<Vec<CellStruct>>,
    grid_nxt:   Vec<Vec<CellStruct>>
}

/* DEFINIÇÃO DOS MÉTODOS EXCLUSIVOS DA ESTRUTURA DE GRADE */
impl Grid {
    fn new (x_dim: usize, y_dim: usize) -> Grid {
        /*
         *          RESUMO:
         *
         *  Método construtuor para a estrutura de
         *  grade com inicialização aleatória para
         *  a tela inicial.
         *
         */ 

        Grid {
            grid:       CellStruct::init_rand(x_dim, y_dim),
            grid_nxt:   vec![vec![CellStruct::new(); y_dim]; x_dim],
        }
    }

    fn calc_cell_state (&mut self, idx: i32, 
                        idy: i32, x_dim: usize, y_dim: usize) {
        /*
         *          RESUMO:
         *
         *  Método de cálculo de cada célula. 
         *  Avalia o estado de forma a determinar
         *  a conformação futura da grade.
         *
         */ 

        let mut num_nei: i32 = 0;
        let cell: CellStruct = self.grid[idx as usize][idy as usize];
        for i in (idx - 1) ..= (idx + 1) {
            for j in (idy - 1) ..= (idy + 1) {
                match (i, j) {
                    tup if tup == (idx, idy) => continue,
                    (_, _) => {
                        let ii: usize = (((i as usize) % x_dim) + x_dim) % x_dim;
                        let jj: usize = (((j as usize) % y_dim) + y_dim) % y_dim;
                        if self.grid[ii][jj].state {
                            num_nei += 1;
                        }
                    }
                }
            }
        }

        if cell.state {
            match num_nei {
                2 | 3   => self.grid_nxt[idx as usize][idy as usize].state = true,
                _       => self.grid_nxt[idx as usize][idy as usize].state = false,
            }
        } else {
            match num_nei {
                3 => self.grid_nxt[idx as usize][idy as usize].state = true,
                _ => self.grid_nxt[idx as usize][idy as usize].state = false,
            }
        }
    }

    fn update_grid (&mut self) {
        /*
         *          RESUMO:
         *
         *  Método de atualização de grade
         *  com padrão calculado baseado
         *  na geração anterior.
         *
         */ 

        self.grid = self.grid_nxt.clone();
    }

    fn print_grid (&mut self) {
        /*
         *          RESUMO:
         *
         *  Método de impressão da estrutura
         *  da grade para visualização básica
         *  no terminal.
         *
         */ 

        for row in self.grid.iter() {
            for cell in row.iter() {
                print!("{}", if cell.state == true { "*" } else { " " } );
            }

            println!();
        }
    }
}

fn clear_screen () {
    /*
     *          RESUMO:
     *
     *  Função de limpeza da saída
     *  padrão para atualização
     *  do terminal em cada 
     *  geração.
     *
     */ 

    print!("\x1B[2J\x1B[1;1H");
}

fn set_dims () -> (usize, usize) {
    /*
     *          RESUMO:
     *
     *  Função de atualização das
     *  dimensões da grade de células,
     *  dessa forma o código se adapta.
     *
     */ 

    if let Some((w, l)) = term_size::dimensions() {
        (l - 2, w)
    } else {
        println!("Aviso!! Não foi possível identificar a definição.");
        (40, 40)
    }
}

fn main () {
    let (x_dim, y_dim): (usize, usize) = set_dims();
    let mut canvas: Grid = Grid::new(x_dim, y_dim);
    let mut gen: u32 = 0;
    loop {
        for idx in 0 .. x_dim {
            for idy in 0 .. y_dim {
                canvas.calc_cell_state(idx as i32, idy as i32, x_dim, y_dim);
            }
        }

        clear_screen();
        canvas.print_grid();
        println!("Generation: {}", gen);
        canvas.update_grid();
        gen += 1;
        thread::sleep(time::Duration::from_millis(500));
    }
}
