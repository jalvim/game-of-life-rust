# Simple implementation of the Conway's Game of life in Rust

All cargo dependencies are configured and exposed inside the configuration files. No GUI framework or library needed except from the standard ones. The UI should be adaptive based on the TTY dimensions.
